package com.syed.Heap;

import java.util.Arrays;

class MinHeap {
	
	int capacity = 20;
	int size = 0;
	int value[] = new int[capacity];
	
	private int getLeftChildIndex(int index){ return index*2+1; }
	private int getRightChildIndex(int index){ return index*2+2; }
	private int getParentIndex(int index){ return (index-1)/2;}
	
	private boolean hasLeftChild(int index){ return getLeftChildIndex(index) < size; }
	private boolean hasRightChild(int index){ return getRightChildIndex(index) < size; }
	private boolean hasParent(int index){ return getParentIndex(index) < size; }
	
	private int leftChild(int index) { return value[getLeftChildIndex(index)]; }
	private int RightChild(int index) { return value[getRightChildIndex(index)]; }
	private int parent(int index) { return value[getParentIndex(index)]; }
	
	
	private void swap(int indexOne, int indexTwo){
		int temp = value[indexOne];
		value[indexOne] = value[indexTwo];
		value[indexTwo] = temp;
	}
	
	
	private void ensureCapacity(){
		if(size == capacity){
			Arrays.copyOf(value, capacity*2);
			capacity *= 2;
		}
	}
	
	public int peek(){
		if(size == 0 ){ throw new IllegalStateException("No Element In Heap");}
		return value[0];
	}
	
	
	//extact the minimum element and remove it from the array
	public int poll(){
		if(size == 0 ){ throw new IllegalStateException("No Element In Heap");}
		int temp = value[0];
		value[0] = value[size-1];
		value[size-1] = 0;
		heapifyDown();
		size--;
		return temp;
	}
	
	public void add(int item){
		value[size] = item; 
		size++;
		heapifyUp();
		ensureCapacity();
	}
	
	public void heapifyUp(){
		int index = size-1;
		while(hasParent(index) && value[index] < parent(index)){
			swap(index, parent(	index));
			index = getParentIndex(index);
		}
	}
	
	public void heapifyDown(){
		int index = 0;
		while(hasLeftChild(index)){
			int smallestIndex = getLeftChildIndex(index);
			if(hasRightChild(index) && RightChild(index) < leftChild(index))
				smallestIndex = getRightChildIndex(index);
			if(value[index] < value[smallestIndex])
				break;
			else{
				swap(index, smallestIndex);
			}
			index = smallestIndex;
		}
	}
	
	public int[] values() {
		return value;
		
	}
	
	
}

public class MinHeapDemo{
	
	public static void main(String arg[]){
		MinHeap minHeap = new MinHeap();
		for(int i=1; i<=10; i++){
			minHeap.add(i);
		}
		
		System.out.println(Arrays.toString(minHeap.value));
		System.out.println(minHeap.poll());
		System.out.println(Arrays.toString(minHeap.value));
		System.out.println(minHeap.poll());
		System.out.println(Arrays.toString(minHeap.value));
	}
	
	
}
