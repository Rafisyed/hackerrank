package com.syed.data;

public class Tree {
	Tree left, right;
	int data;
	public Tree(int data) {
		this.data = data;
	}
	
	public void insert(int value){
		if(value<= data){
			if(left == null)
				left = new Tree(value);
			else
				left.insert(value);
		}
		else{
			if(right == null)
				right = new Tree(value);
			else{
				right.insert(value);
			}
		}
	}
}
