package com.syed.data;

public class Stack {
	private class Node{
		char data;
		Node next;
		
		Node(char data){
			this.data = data;
			this.next = null;
		}
		Node(Node node){
			if(node != null){
				this.data = node.data;
				this.next = node.next;
			}
		}
	}
	public Stack() {
	}
	
	Node top; //add or remove here 
	
	private boolean isEmpty(){
		return top==null;
	}
	
	public String push(char data){
		Node newNode = new Node(data);
		if(!isEmpty())
			newNode.next=top;
		top = newNode;
		return "added "+data;
	}
	
	public String pop(){
		if(isEmpty())
			return "Stack is empty";
		else{
			String data = "removed "+top.data;
			top = top.next;
			return data;
		} 	
	}
	
	public String toString(){
		StringBuffer buffer = new StringBuffer();
		Node current = new Node(top);
		while(current.next!=null){
			buffer.append(current.data+" ");
			current = current.next;
			
		}
		buffer.append(current.data);
		return buffer.toString();
	}
	
}

