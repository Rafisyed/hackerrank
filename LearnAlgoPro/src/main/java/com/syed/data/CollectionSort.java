package com.syed.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class Holder {
	private int value;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Holder(int value) {
		this.value = value;
	}
	
	public String toString(){
		return Integer.toString(this.value);
		
	}

	
}

public class CollectionSort { 
	public static void main(String[] args) {
		List<Holder> temp = new ArrayList<>(
				Arrays.asList(new Holder(1), new Holder(2), new Holder(5), new Holder(3), new Holder(4)));

		System.out.println(temp.toString());

		Collections.sort(temp, (Holder h1, Holder h2) -> h1.getValue()-h2.getValue());
		
		for (Holder holder : temp) {
			System.out.print(holder.toString()+" ");
		}
		
	}
}
