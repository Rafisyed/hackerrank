package com.syed.solutions;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

class Queue<E> {
	
	class Node<E>{
		E value;
		Node<E> next;
		Node(E t){
			this.value=t;
		}
	}
	
	Node<E> head;
	Node<E> tail;
	
	E peekFront(){
		if(head == null) // needs review 
			throw new NoSuchElementException();
		return head.value;
		
	}
	
	E peekLast(){
		if(tail == null) // needs review
			throw new NoSuchElementException();
		return tail.value;
	}
	
	// insert element at the end/tail of the Queue
	void enqueue(E data){
		if(head == null)
			head = new Node<E>(data);
		else if(tail == null){
			tail = new Node<E>(data);
			head.next = tail;
		}
		else{
			Node<E> node = new Node<E>(data);
			tail.next = node;
			tail = node;
		}
	}
	
	
	// remove element at front/head of Queue
	E dequeue(){
		if(isEmpty())
			throw new NoSuchElementException();
		Node<E> node = head;
		head = head.next;	
		if(head.next == null)
			tail = null;
		return node.value;
	}
	
	boolean isEmpty(){
		if(head == null)
			return true;
		return false;
	}
	List<E> printQueue(){
		if(isEmpty())
			throw new NoSuchElementException();
		Node<E> temp = head;
		List<E> values = new ArrayList<E>();
		while(temp.next!=null){
			values.add(temp.value);
			temp = temp.next;
		}
		values.add(temp.value);
		return values;
	}
	
}
public class QueueDemo{
	
	public static void main(String[] args){
		Queue<Integer> queue = new Queue<Integer>();
		System.out.println(queue.isEmpty());
		for (int i = 1; i <= 10; i++) {
			queue.enqueue(i);

		}
		System.out.println(queue.printQueue());
		System.out.println(queue.isEmpty());
		System.out.println(queue.printQueue());
		System.out.println(queue.peekFront());
		System.out.println(queue.peekLast());
		System.out.println(queue.dequeue());
		System.out.println(queue.dequeue());
		System.out.println(queue.dequeue());
		System.out.println(queue.printQueue());

		
	}
}
