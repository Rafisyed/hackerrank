package com.syed.Tries;

import java.util.HashMap;
import java.util.Map;

class Node {
	Map<Character, Node> children;
	boolean isWord;
	int count;

	public Node() {
	}
	public Node(Character ch) {
		this.children.put(ch, null);
	}
}

public class TriesTest {
	public static void main(String args[]) {
		// insert the word "hello" in tries
		Node root = new Node();
		insertWordInTies("hellow", root);
		insertWordInTies("hema", root);
		insertWordInTies("hai", root);
		System.out.println("he = "+search("he", root));
		System.out.println("hai = "+search("hai", root));
		System.out.println("hem = "+search("hem", root));
		System.out.println("hay = "+search("hay", root));
		
	}

	private static boolean search(String string, Node root) {
		Node currentNode = root;
		if(currentNode == null || currentNode.children == null || currentNode.children.isEmpty())
			return false;
		for(int i=0; i<string.length(); i++){
			Character ch = string.charAt(i);
			if(currentNode.children.containsKey(ch)){
				currentNode = currentNode.children.get(ch);
			}
			else
				return false;
		}
		System.out.println(string+"=="+currentNode.count);
		return true;
	}

	private static void insertWordInTies(String word, Node root) {

		Node currentNode = root;

		for (int i = 0; i < word.length(); i++) {
			Character ch = word.charAt(i);
			if (currentNode.children == null) {
				Map<Character, Node> temp = new HashMap<Character, Node>();
				temp.put(ch, new Node());
				currentNode.children = temp;
				currentNode = currentNode.children.get(ch);
				currentNode.count++;
			}else if(!currentNode.children.containsKey(ch)){
				currentNode.children.put(ch, new Node());
				currentNode = currentNode.children.get(ch);
				currentNode.count++;
			}
			else {
				currentNode = currentNode.children.get(ch);
				currentNode.count++;
			}
		}
		currentNode.isWord = true;
	}
}
