package com.syed.data;

public class StackImpl {

	public static void main(String[] args) {
		Stack stack = new Stack();
		String data = "{[()]}";
		for (int i=0; i<data.length(); i++) {
			System.out.println(stack.push(data.charAt(i)));
		}
		System.out.println(stack.toString());
		for (int i=0; i<data.length(); i++) {
			System.out.println(stack.pop());
		}
		System.out.println(stack.toString());
	}

}
